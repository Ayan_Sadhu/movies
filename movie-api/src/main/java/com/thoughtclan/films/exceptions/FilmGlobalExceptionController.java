package com.thoughtclan.films.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class FilmGlobalExceptionController {

	
	@ExceptionHandler
	public ResponseEntity<FilmErrorResponse> handleException(FilmNotFoundException exc){
		
		//create a error response
		FilmErrorResponse error=new FilmErrorResponse();
		error.setStatus(HttpStatus.NOT_FOUND.value());
		error.setMessage(exc.getMessage());
		error.setTimestamp(System.currentTimeMillis());
		
		
		
		//reuturn response entity
		
		return new ResponseEntity<FilmErrorResponse>(error,HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler
	public ResponseEntity<FilmErrorResponse> handleException(Exception exc){
		//create a error response
		FilmErrorResponse error=new FilmErrorResponse();
		error.setStatus(HttpStatus.BAD_REQUEST.value());
		error.setMessage(exc.getMessage());
		error.setTimestamp(System.currentTimeMillis());
		
		
		
		//reuturn response entity
		
		return new ResponseEntity<FilmErrorResponse>(error,HttpStatus.BAD_REQUEST);
	}
}
