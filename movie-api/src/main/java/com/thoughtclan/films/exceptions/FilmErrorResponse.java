package com.thoughtclan.films.exceptions;

public class FilmErrorResponse {
	


	private int status;
	private String message;
	 private long timestamp;
	 
	 public FilmErrorResponse() {
		// TODO Auto-generated constructor stub
	}

		public FilmErrorResponse(int status, String message, long timestamp) {
			this.status = status;
			this.message = message;
			this.timestamp = timestamp;
		}

		public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public long getTimestamp() {
			return timestamp;
		}

		public void setTimestamp(long timestamp) {
			this.timestamp = timestamp;
		}
		
		
		
	 
}
