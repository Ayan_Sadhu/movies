package com.thoughtclan.films.exceptions;

public class FilmNotFoundException  extends RuntimeException{

	public FilmNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FilmNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public FilmNotFoundException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public FilmNotFoundException(String message) {
		super(message);
		
	}

	public FilmNotFoundException(Throwable cause) {
		super(cause);
		
	}

	
	
}
