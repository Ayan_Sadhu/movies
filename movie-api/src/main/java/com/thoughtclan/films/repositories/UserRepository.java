package com.thoughtclan.films.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.thoughtclan.films.model.User;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User,Integer>, JpaSpecificationExecutor<User> {

	User findByuserId(int userId);

}
