package com.thoughtclan.films.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.thoughtclan.films.dto.FilmDto;
import com.thoughtclan.films.model.Film;

@Repository
public interface FilmRepository extends PagingAndSortingRepository<Film,Integer>,JpaSpecificationExecutor<Film> {

	Film findByfilmId(int filmId);

	Film findByfilmName(String filmName);

	

	
}
