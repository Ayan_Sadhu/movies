package com.thoughtclan.films.services;

import java.util.List;

import com.thoughtclan.films.dto.AddFilmDto;
import com.thoughtclan.films.dto.FilmDto;
import com.thoughtclan.films.model.Film;

public interface FilmServices {

	 List<Film> getFilms();

	Film getFilm(int filmId);

	FilmDto addFilm(AddFilmDto addfilmDto);

}
