package com.thoughtclan.films.services.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thoughtclan.films.dto.FilmDto;
import com.thoughtclan.films.model.Film;
import com.thoughtclan.films.model.User;
import com.thoughtclan.films.repositories.FilmRepository;
import com.thoughtclan.films.repositories.UserRepository;
import com.thoughtclan.films.services.UserServices;

@Service
public class UserServicesImpl implements UserServices {

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private FilmRepository filmRepo;

	@Override
	public List<User> getAllUser() {

		return (List<User>) userRepo.findAll();
	}

	@Override
	public User getUser(int userId) {

		return userRepo.findByuserId(userId);
	}

	@Override
	public void deleteMovie(int userId, int filmId) {
		User user = userRepo.findByuserId(userId);
		Collection<Film> films = user.getFilm();
		Film film = filmRepo.findByfilmId(filmId);
		if (films.contains(film))
			films.remove(film);
		int addmovie = film.getCount() - 1;
		film.setCount(addmovie);
		user.setFilm(films);
		userRepo.save(user);

	}

	@Override
	public User addMovie(FilmDto filmDto, int userId) {
		List<Film> f1 = new ArrayList<Film>();
		Film film = filmRepo.findByfilmName(filmDto.getFilmName());
		f1.add(film);
		User user = userRepo.findByuserId(userId);

		if (!(user.getFilm().contains(film)))
			user.getFilm().add(film);

		int addcount = film.getCount() != null ? film.getCount() + 1 : 1;
		film.setCount(addcount);
		filmRepo.save(film);
		userRepo.save(user);
		return user;
	}

	@Override
	public Collection<Film> getWatchList(int userId) {
		User user=userRepo.findByuserId(userId);
		Collection<Film> film=user.getFilm();
		return film;
	}

}
