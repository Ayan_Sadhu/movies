package com.thoughtclan.films.services.impl;

import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thoughtclan.films.dto.AddFilmDto;
import com.thoughtclan.films.dto.FilmDto;
import com.thoughtclan.films.dto.UserDto;
import com.thoughtclan.films.model.Film;
import com.thoughtclan.films.model.User;
import com.thoughtclan.films.repositories.FilmRepository;
import com.thoughtclan.films.repositories.UserRepository;
import com.thoughtclan.films.services.FilmServices;

@Service
@Transactional
public class FilmServicesImpl implements FilmServices {

	@Autowired
	private FilmRepository filmrepo;

	@Autowired
	private UserRepository userRepo;

	@Override
	public List<Film> getFilms() {
		return (List<Film>) filmrepo.findAll();
	}

	@Override
	public Film getFilm(int filmId) {
		return filmrepo.findByfilmId(filmId);
	}


	@Override
	public FilmDto addFilm(AddFilmDto addfilmDto) {
		Film film = new Film(addfilmDto.getFilmName());
		film.setFilmGenre(film.getFilmGenre());
		film.setFilmStudio(film.getFilmStudio());
		film.setRating(film.getRating());
		film.setReleaseYear(film.getReleaseYear());
		film.setUrl(film.getUrl());
		User user = new User();
		user.setUserId(addfilmDto.getUserId());
		film.setUser((Collection<User>) user);

		User userToUpdate = userRepo.findByuserId(addfilmDto.getUserId());
		int updatedNumFilms = userToUpdate.getCountFilm() != null ? 
				userToUpdate.getCountFilm().intValue() + 1 : 1;


		Film createFilm = filmrepo.save(film);
		return convertToFilmDto(createFilm, false);
	}

	private FilmDto convertToFilmDto(Film film, boolean b) {
		FilmDto filmDto = new FilmDto(film.getFilmName());
		filmDto.setFilmId(film.getFilmId());
		filmDto.setFilmName(film.getFilmName());
		filmDto.setFilmGenre(film.getFilmGenre());
		filmDto.setFilmStudio(film.getFilmStudio());
		filmDto.setRating(film.getRating());
		filmDto.setReleaseYear(film.getReleaseYear());
		filmDto.setUrl(film.getUrl());

		if (b) {

			User user = (User) film.getUser();
			UserDto userDto = new UserDto();
			userDto.setUserEmail(user.getUserEmail());
			userDto.setUserId(user.getUserId());
			userDto.setUserName(user.getUserName());
			userDto.setCountFilm(user.getCountFilm());
			filmDto.setUserList(userDto);

		}
		return filmDto;
	}

}
