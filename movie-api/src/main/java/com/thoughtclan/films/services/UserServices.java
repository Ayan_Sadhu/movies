package com.thoughtclan.films.services;

import java.util.Collection;
import java.util.List;

import com.thoughtclan.films.dto.FilmDto;
import com.thoughtclan.films.dto.UserDto;
import com.thoughtclan.films.model.Film;
import com.thoughtclan.films.model.User;

public interface UserServices {

	List<User> getAllUser();

	User getUser(int userId);

	void deleteMovie(int userId, int filmId);

	User addMovie(FilmDto filmDto, int userId);

	Collection<Film> getWatchList(int userId);

}
