package com.thoughtclan.films.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
@Component
@Aspect
public class FilmServiceAspect {
	
		private static final Logger logger = LoggerFactory.getLogger(FilmServiceAspect.class);
		
		@Before("execution(* com.thoughtclan.films.services.impl.FilmServicesImpl.*(..))")
		public void getNameAdvice(){
			logger.info("Executing Advice on FilmServicesImpl");
		}
	}


