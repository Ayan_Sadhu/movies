package com.thoughtclan.films.model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table
public class User {

	@Id
	private int userId;
	
	@Column
	private String userName;
	
	@Column
	private String userEmail;
	
	@Column
	private String password;
	
	@Column
	private Integer countFilm;


	public Integer getCountFilm() {
		return countFilm;
	}

	public void setCountFilm(Integer countFilm) {
		this.countFilm = countFilm;
	}

	@ManyToMany(fetch=FetchType.LAZY)
	public Collection<Film> film;
	
	
	public Collection<Film> getFilm() {
		return film;
	}

	public void setFilm(Collection<Film> film) {
		this.film = film;
	}

	public User() {
		
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
