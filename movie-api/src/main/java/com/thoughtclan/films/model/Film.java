package com.thoughtclan.films.model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="filmtable")
public class Film {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int filmId;
	
	
	@Column
	private String filmName;
	
	

	@Column
	private String filmGenre;
	
	@Column
	private String filmStudio;
	
	@Column
	private int rating;
	
	@Column(name="releaseYear")
	private int releaseYear;
	
	@Column
	private String url;
	
	@Column
	public Integer countUser;
	
	@ManyToMany(fetch=FetchType.LAZY)
	public Collection<User> user;
	
	

	
	public Collection<User> getUser() {
		return user;
	}

	public void setUser(Collection<User> user) {
		this.user = user;
	}

	public Film() {
				
	}

	public Film(String filmName2) {
		this.filmName=filmName2;
	}

	public int getFilmId() {
		return filmId;
	}

	public void setFilmId(int filmId) {
		this.filmId = filmId;
	}
	
	public String getFilmName() {
		return filmName;
	}

	public void setFilmName(String filmName) {
		this.filmName = filmName;
	}

	public String getFilmGenre() {
		return filmGenre;
	}

	public void setFilmGenre(String filmGenre) {
		this.filmGenre = filmGenre;
	}

	public String getFilmStudio() {
		return filmStudio;
	}

	public void setFilmStudio(String filmStudio) {
		this.filmStudio = filmStudio;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public int getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(int releaseYear) {
		this.releaseYear = releaseYear;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getCount() {
		return countUser;
	}

	public void setCount(Integer count) {
		this.countUser = count;
	}
	
}
