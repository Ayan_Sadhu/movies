package com.thoughtclan.films.dto;

public class AddFilmDto {
	
	private int userId;
	private String filmName;
	
	public AddFilmDto() {
		
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getFilmName() {
		return filmName;
	}

	public void setFilmName(String filmName) {
		this.filmName = filmName;
	}

	@Override
	public String toString() {
		return "AddFilmDto [userId=" + userId + ", filmName=" + filmName + "]";
	}
	
	

}
