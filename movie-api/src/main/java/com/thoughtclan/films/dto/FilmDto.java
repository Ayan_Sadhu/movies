package com.thoughtclan.films.dto;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class FilmDto {

	private int filmId;
	private String filmName;

	private String filmGenre;
	private String filmStudio;
	private int rating;
	private int releaseYear;
	private String url;
	private Integer countUser;
	public Collection<UserDto> userList;

	public Collection<UserDto> getUserList() {
		return userList;
	}

	public void setUserList(UserDto userDto) {
		this.userList = (Collection<UserDto>) userDto;
	}

	public Integer getCount() {
		return countUser;
	}

	public void setCount(Integer count) {
		this.countUser = count;
	}

	public FilmDto() {

	}

	public FilmDto(String filmName2) {
	this.filmName=filmName2;
	}

	public int getFilmId() {
		return filmId;
	}

	public void setFilmId(int filmId) {
		this.filmId = filmId;
	}

	public String getFilmName() {
		return filmName;
	}

	public void setFilmName(String filmName) {
		this.filmName = filmName;
	}

	public String getFilmGenre() {
		return filmGenre;
	}

	public void setFilmGenre(String filmGenre) {
		this.filmGenre = filmGenre;
	}

	public String getFilmStudio() {
		return filmStudio;
	}

	public void setFilmStudio(String filmStudio) {
		this.filmStudio = filmStudio;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public int getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(int releaseYear) {
		this.releaseYear = releaseYear;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "FilmDto [filmId=" + filmId + ", filmGenre=" + filmGenre + ", filmStudio=" + filmStudio + ", rating="
				+ rating + ", releaseYear=" + releaseYear + ", url=" + url + "]";
	}

}
