package com.thoughtclan.films.dto;

import java.util.Collection;

public class UserDto {

	private int userId;
	private String userName;
	private String userEmail;
	private String password;
	private Integer countFilm;
	
	public Integer getCountFilm() {
		return countFilm;
	}

	public void setCountFilm(Integer countFilm) {
		this.countFilm = countFilm;
	}

	public Collection<FilmDto> filmList;
	
	public UserDto() {
		
	}

	public Collection<FilmDto> getFilmList() {
		return filmList;
	}

	public void setFilmList(Collection<FilmDto> filmList) {
		this.filmList = filmList;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "UserDto [userId=" + userId + ", userName=" + userName + ", userEmail=" + userEmail + ", password="
				+ password + "]";
	}
	
	
}
