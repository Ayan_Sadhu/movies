package com.thoughtclan.films.controller;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.thoughtclan.films.dto.FilmDto;
import com.thoughtclan.films.dto.UserDto;
import com.thoughtclan.films.model.Film;
import com.thoughtclan.films.model.User;
import com.thoughtclan.films.repositories.UserRepository;
import com.thoughtclan.films.services.FilmServices;
import com.thoughtclan.films.services.UserServices;
import com.thoughtclan.films.services.impl.UserServicesImpl;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private UserServices userService;

	@Autowired
	private FilmServices filmService;

	@GetMapping("/list")
	public List<User> getAllUser() {

		return userService.getAllUser();
	}

	@GetMapping("/{userId}/wl")
	public Collection<Film> getFromWl(@PathVariable int userId) {

		return userService.getWatchList(userId);

	}

	@PostMapping("/list")
	public User addUser(@RequestBody UserDto userDto) {
		User user = new User();
		user.setUserName(userDto.getUserName());
		user.setPassword(userDto.getPassword());
		user.setUserEmail(userDto.getUserEmail());
		user.setCountFilm(userDto.getCountFilm());
		userRepo.save(user);
		return user;
	}

	@PutMapping("/list/{userId}")
	public User addFilmToUser(@RequestBody FilmDto filmDto, @PathVariable int userId) {

		return userService.addMovie(filmDto, userId);
	}

	@DeleteMapping("/{userId}/delete/{filmid}")
	public String deleteFilmForUser(@RequestBody Film film, @RequestBody User user,
			@PathVariable int userId, @PathVariable int filmId) {

		film = filmService.getFilm(filmId);
		user = userService.getUser(userId);

		userService.deleteMovie(userId, filmId);
		return "movie " + film.getFilmName() + " delete from user " + user.getUserName();
	}

}
