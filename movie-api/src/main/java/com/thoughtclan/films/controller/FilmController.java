package com.thoughtclan.films.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.thoughtclan.films.dto.AddFilmDto;
import com.thoughtclan.films.dto.FilmDto;
import com.thoughtclan.films.exceptions.FilmNotFoundException;
import com.thoughtclan.films.model.Film;
import com.thoughtclan.films.repositories.FilmRepository;
import com.thoughtclan.films.services.FilmServices;

@Controller
@RequestMapping("/film")
public class FilmController {

	@Autowired
	private FilmServices filmServices;

	@Autowired
	private FilmRepository filmRepo;

	@GetMapping
	public List<Film> getAllFilm() {

		List<Film> filmList = filmServices.getFilms();

		return filmList;

	}

	@PostMapping
	public FilmDto addFilm(@Validated @RequestBody AddFilmDto addfilmDto) {
		/*
		 * Film Film = new Film(); Film.setFilmName(filmDto.getFilmName());
		 * Film.setFilmGenre(filmDto.getFilmGenre());
		 * Film.setFilmStudio(filmDto.getFilmStudio());
		 * Film.setRating(filmDto.getRating());
		 * Film.setReleaseYear(filmDto.getReleaseYear()); Film.setUrl(filmDto.getUrl());
		 * Film.setCount(filmDto.getCount());
		 */
		return filmServices.addFilm(addfilmDto);

	

	}


}
